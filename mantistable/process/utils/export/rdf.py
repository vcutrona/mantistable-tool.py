import rdflib
from django.core.exceptions import ObjectDoesNotExist

from mantistable.process.utils.export.exporter import Exporter


class RDFExporter(Exporter):
    def __init__(self, format: str, tables: list):
        self.format = format
        self.tables = tables

        if self.format not in ['xml', 'nt', 'n3', 'turtle', 'json-ld']:
            raise ValueError()

    def export(self):
        linkings = []
        for table in self.tables:
            try:
                table.linking
                linkings.append(table)
            except ObjectDoesNotExist:
                pass

        graph = rdflib.Graph()
        for table in linkings:
            for triple in self.export_table(table):
                graph.add(triple)

        return graph.serialize(format=self.format).decode("utf-8")

    def export_table(self, table):
        dbo = rdflib.Namespace("http://dbpedia.org/ontology/")
        triples = []
        num_cols = table.num_cols
        num_rows = table.num_rows

        subject_idx = table.infotable.subject_col
        necols_idxs = {
            necol["index"]: necol
            for necol in table.infotable.ne_cols
        }
        litcols_idxs = {
            litcol["index"]: litcol
            for litcol in table.infotable.lit_cols
        }

        # rdf:type
        for col_idx, col in enumerate(table.tabledata.data):
            if col_idx in necols_idxs:
                for row_idx, row in enumerate(col):
                    concept = necols_idxs[col_idx]["type"]
                    if "linked_entity" in row and row["linked_entity"] != "" and concept != "":
                        subject = rdflib.URIRef(row["linked_entity"])
                        predicate = rdflib.RDF.type
                        object = rdflib.URIRef(concept)

                        triples.append((subject, predicate, object))

        # predicates:
        for row_idx in range(0, num_rows):
            for col_idx in range(0, num_cols):
                if col_idx == subject_idx:
                    continue

                subject_row = table.tabledata.data[subject_idx][row_idx]
                row = table.tabledata.data[col_idx][row_idx]

                if "linked_entity" in subject_row and subject_row["linked_entity"] != "":
                    subject = rdflib.URIRef(subject_row["linked_entity"])

                    if col_idx in necols_idxs and "rel" in necols_idxs[col_idx] and necols_idxs[col_idx]["rel"] != "" \
                            and "linked_entity" in row and row["linked_entity"] != "":

                        predicate = rdflib.URIRef(dbo[necols_idxs[col_idx]["rel"]])
                        object = rdflib.URIRef(row["linked_entity"])
                    elif col_idx in litcols_idxs and "rel" in litcols_idxs[col_idx] and litcols_idxs[col_idx]["rel"] != "":
                        rel = litcols_idxs[col_idx]["rel"]
                        if rel.startswith("/"):
                            rel = rel[1:]
                        predicate = rdflib.URIRef(dbo[rel])

                        # very simple and wrong way to get datatype
                        xsd = litcols_idxs[col_idx]["data_type"]
                        if isinstance(xsd, list):
                            xsd = xsd[0]
                            if isinstance(xsd, list):
                                xsd = xsd[0]

                        xsd = xsd["uri"]

                        value = row["value_original"]
                        object = rdflib.Literal(value, datatype=xsd)
                    else:
                        continue

                    triples.append((subject, predicate, object))

        return triples


""" DEPRECATED
def getMongoData():
    # db connection
    client = MongoClient('mongo', 27021)
    db = client['mantistable']

    # main dict to contain mongodb documents
    index_link = {"id": {}, "sub_id": {}, "links": {}, "sub_links": {}, "attr_id": {}, "attrs": {}}

    # get infotable
    collection = db['mantistable_infotable']
    docs_iterable = collection.find({})
    for document in docs_iterable:
        data = document['ne_cols']
        for num in data:
            if not ("rel" in num):
                if num["type"] is not None and num["type"] != "":
                    index_link["id"][num["index"]] = num["type"]
                    index_link["links"][num["index"]] = list()
            else:
                if num["rel"] is not None and num["rel"] != "":
                    index_link["sub_id"][num["index"]] = num["rel"]
                    index_link["sub_links"][num["index"]] = list()

        data = document['lit_cols']
        for num in data:
            if num["rel"] is not None:
                index_link["attr_id"][num["index"]] = num["rel"]
                index_link["attrs"][num["index"]] = list()

        data = document['no_ann_cols']
        for num in data:
            if num["header"] is not None:
                index_link["attr_id"][num["index"]] = num["header"]
                index_link["attrs"][num["index"]] = list()

    # get datatable
    collection = db['mantistable_tabledata']
    docs_iterable = collection.find({})
    for document in docs_iterable:
        data = document['data']
        for index in index_link["id"].keys():
            for i in range(len(data[index])):
                if "linked_entity" in data[index][i] and data[index][i]["linked_entity"] is not None and data[index][i]["linked_entity"] != "null":
                    index_link["links"][index].append({"id": i, "value": data[index][i]["linked_entity"]})

        for index in index_link["sub_id"].keys():
            for i in range(len(data[index])):
                if "linked_entity" in data[index][i] and data[index][i]["linked_entity"] is not None and data[index][i]["linked_entity"] != "null":
                    index_link["sub_links"][index].append({"id": i, "value": data[index][i]["linked_entity"]})

        for index in index_link["attr_id"].keys():
            for i in range(len(data[index])):
                if "value_original" in data[index][i] and data[index][i]["value_original"] is not None and data[index][i]["value_original"] != "null":
                    index_link["attrs"][index].append({"id": i, "value": data[index][i]["value_original"]})

    return index_link


def mongo2rdf(index_link):
    # load rdf header and footer
    # NOTE: use Assets().get_asset("export/intro.txt") instead
    with open("mantistable/private/export/intro.txt", "r", encoding="utf-8", newline="\r\n") as f:
        intro = f.read()
    with open("mantistable/private/export/outro.txt", "r", encoding="utf-8", newline="\r\n") as f:
        outro = f.read()

    # data scan and rdf string build
    rdf = intro
    for index in index_link["id"].keys():
        for item in index_link["links"][index]:
            rdf += '\r\n    <rdf:Description rdf:about="' + item["value"] + '">'
            rdf += '\r\n        <rdf:type rdf:resource="' + index_link["id"][index] + '" />'
            rdf += '\r\n        <dbo:columnId rdf:datatype="http://www.w3.org/2001/XMLSchema#integer">' + str(
                index) + '</dbo:columnId>'
            rdf += '\r\n        <dbo:rowId rdf:datatype="http://www.w3.org/2001/XMLSchema#integer">' + str(
                item["id"]) + '</dbo:rowId>'
            for sub_id in index_link["sub_id"].keys():
                rdf += '\r\n        <dbo:' + index_link["sub_id"][sub_id][
                                             index_link["sub_id"][sub_id].rfind('/') + 1:] + ' rdf:resource="' + str(
                    [j["value"] for j in index_link["sub_links"][sub_id] if j["id"] == item["id"]][0]) + '" />'
            for attr_id in index_link["attr_id"].keys():
                if index_link["attr_id"][attr_id] != "":
                    rdf += '\r\n        <dbo:' + index_link["attr_id"][attr_id][
                                                index_link["attr_id"][attr_id].rfind('/') + 1:]
                    try:
                        int(str([j["value"] for j in index_link["attrs"][attr_id] if j["id"] == item["id"]][0]).replace(",",
                                                                                                                        ""))
                        rdf += ' rdf:datatype="http://www.w3.org/2001/XMLSchema#integer">'
                    except:
                        try:
                            float(str([j["value"] for j in index_link["attrs"][attr_id] if j["id"] == item["id"]][0]))
                            rdf += ' rdf:datatype="http://www.w3.org/2001/XMLSchema#float">'
                        except:
                            if (str([j["value"] for j in index_link["attrs"][attr_id] if j["id"] == item["id"]][
                                        0]).lower() == "true" or str(
                                [j["value"] for j in index_link["attrs"][attr_id] if j["id"] == item["id"]][
                                    0]).lower() == "false"):
                                rdf += ' rdf:datatype="http://www.w3.org/2001/XMLSchema#bool">'
                            else:
                                rdf += ' rdf:datatype="http://www.w3.org/2001/XMLSchema#string">'
                    rdf += str([j["value"] for j in index_link["attrs"][attr_id] if j["id"] == item["id"]][0])
                    rdf += '</dbo:' + index_link["attr_id"][attr_id][index_link["attr_id"][attr_id].rfind('/') + 1:] + '>'
            rdf += '\r\n    </rdf:Description>'
    rdf += outro

    # debug output
    '''
    with open("out.xml", "w+", encoding="utf-8", newline="\n") as f:
        f.write(rdf)
    '''

    # build rdf graph
    graph = rdflib.Graph()
    graph.parse(data=rdf, format='xml')

    return graph


def exportXml(graph):
    # with open("conversions/xml.xml", "w+", encoding="utf-8", newline="\r\n") as f:
    # f.write(graph.serialize(format='xml').decode("utf-8"))
    return graph.serialize(format='xml').decode("utf-8")


def exportNt(graph):
    # with open("conversions/nt.nt", "w+", encoding="utf-8", newline="\r\n") as f:
    # f.write(graph.serialize(format='nt').decode("utf-8"))
    return graph.serialize(format='nt').decode("utf-8")


def exportN3(graph):
    # with open("conversions/n3.n3", "w+", encoding="utf-8", newline="\r\n") as f:
    # f.write(graph.serialize(format='n3').decode("utf-8"))
    return graph.serialize(format='n3').decode("utf-8")


def exportTurtle(graph):
    # with open("conversions/turtle.ttl", "w+", encoding="utf-8", newline="\r\n") as f:
    # f.write(graph.serialize(format='turtle').decode("utf-8"))
    return graph.serialize(format='turtle').decode("utf-8")


def exportJsonLd(graph):
    # with open("conversions/json-ld.jsonld", "w+", encoding="utf-8", newline="\r\n") as f:
    # f.write(graph.serialize(format='json-ld').decode("utf-8"))
    return graph.serialize(format='json-ld').decode("utf-8")


def exportRdf(exportType):
    mongoData = getMongoData()
    graph = mongo2rdf(mongoData)
    if exportType == 'xml':
        return exportXml(graph)
    elif exportType == 'nt':
        return exportNt(graph)
    elif exportType == 'n3':
        return exportN3(graph)
    elif exportType == 'turtle':
        return exportTurtle(graph)
    elif exportType == 'jsonld':
        return exportJsonLd(graph)
"""
