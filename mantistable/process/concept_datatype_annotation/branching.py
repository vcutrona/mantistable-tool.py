import json

import networkx as nx

from mantistable.process.utils.assets.assets import Assets

CC = json.loads(Assets().get_asset("CC.json"))
cc_list = json.loads(Assets().get_asset("CCKeys.json"))
ontology_graph = json.loads(Assets().get_asset("OntologyGraph.json"))


def create_graph():
    node_list = set()
    edge_list = []

    for key in ontology_graph.keys():
        sons = ontology_graph[key]

        for s in sons:
            edge_list.append((key, s))

        node_list.add(key)
        node_list.union(set(sons))

    ont_G = nx.Graph()
    ont_G.add_nodes_from(node_list)
    ont_G.add_edges_from(edge_list)

    return ont_G


def get_branches(winning_concepts_freq, ont_G):
    paths = []

    winning_concepts = winning_concepts_freq.keys()
    node_father = winning_concepts
    winning_concepts = list(filter(lambda concept: concept not in cc_list, winning_concepts))
    node_father = list(set(node_father) - set(winning_concepts))

    if len(winning_concepts) == 0:
        for node in node_father:
            paths.append([node])

    # 1
    sources = []
    for wc in winning_concepts:
        source = None
        for cc_key in CC.keys():
            if wc in CC[cc_key]:
                source = cc_key
                sources.append(source)
                break

        assert (source is not None)

        try:
            path = nx.shortest_path(ont_G, source, wc)
            paths.append(path)
        except nx.NetworkXNoPath:
            pass

    # 2: Group by cc
    cc_branches = {}
    for path in paths:
        source = path[0]
        if source in cc_branches.keys():
            cc_branches[source].append(path[1:])
        else:
            cc_branches[source] = [path[1:]]

    # 3: ArgMax
    cc_freq = {}
    for cc_key in cc_branches.keys():
        concepts = set([cc_key] + [item for sublist in cc_branches[cc_key] for item in sublist])
        cc_freq[cc_key] = sum([winning_concepts_freq.get(cname, 1) for cname in concepts])

    max_freq = max(cc_freq.values())
    max_source = [source for source in cc_freq.keys() if cc_freq[source] == max_freq][0]

    # 4: return winning paths
    return [
        tuple([max_source] + subpath)
        for subpath in cc_branches[max_source]
    ]


def get_annotation(concepts, ont_G):
    assert (len(concepts) > 0)

    branches = get_branches(concepts, ont_G)
    #max_len_branch = max([len(branch) for branch in branches])
    #max_branch = [branch for branch in branches if len(branch) == max_len_branch][0]
    """ branches = [ 
        tuple([
            concept 
            for concept in branch 
            if concept != "Thing" and concept != "Agent"
        ])
        for branch in branches
    ]
    freq_branches = {} """
    
    """ for branch in branches:
        if len(branch) == 1:
            freq_branches[branch] = concepts.get(branch[0], 1)
        else:
            freq_branches[branch] = concepts.get(branch[0], 1) + concepts.get(branch[-1], 1) """
    
    freq_branches = {
        branch: sum([concepts.get(cname, 1) for cname in branch])
        for branch in branches
    }
    #freq = concepts.get(max_branch[max_len_branch-1], 1) - 1
    """ for branch in freq_branches:
        len_branch = len(branch)
        iteration = max_len_branch - len_branch
        for i in range (0, iteration):
            freq_branches[branch] += freq """ 

    #print("FREQ BRANCHES", freq_branches)
    max_freq = max(freq_branches.values())
    max_branches = [branch for branch in freq_branches.keys() if freq_branches[branch] == max_freq]
    return {
        concept: concepts.get(concept, 1)
        for concept in max_branches[0]
        if concept != "Agent" and concept != "Thing"
    }


"""
def get_annotation(concepts, ontology_ct):
    assert (len(concepts) > 0)

    concepts_name = list(concepts.keys())
    # print(concepts_name)
    freq_branches = {
        branch: sum([concepts[cname] for cname in branch])
        for branch in build_branches(concepts_name, ontology_ct)
    }

    if len(freq_branches.keys()) > 0:
        max_freq = max(freq_branches.values())
        max_branches = [branch for branch in freq_branches.keys() if freq_branches[branch] == max_freq]

        return {
            cname: concepts[cname]
            for branch in max_branches
            for cname in branch
        }
    return {}


def build_branches(list_concepts, ontology_graph_ct):
    branches = [[list_concepts[0]]]
    for i in range (1, len(list_concepts)):   
        insert_concept(branches, list_concepts[i], ontology_graph_ct)
        
    return tuple(
        tuple(branch)
        for branch in branches
    )    


def insert_concept(branches, cpt,  ontology_graph_ct):
    size = len(branches)
    for i in range(0, size):
        concept_rapp = branches[i][0]
        
        if concept_rapp in ontology_graph_ct and cpt in ontology_graph_ct[concept_rapp]:
            branches[i].append(cpt)
            return 
       
        if cpt in ontology_graph_ct and concept_rapp in ontology_graph_ct[cpt]:
            branches[i].append(cpt)
            return 

    branches.append([cpt])    


def get_winning_branch(branch, ontology_graph_ct):
    assert (len(branch.keys()) > 0)

    root = list(branch.keys())[0]
    for concept in branch.keys():
        if root != concept:
            if concept in ontology_graph_ct.keys() and root in ontology_graph_ct[concept]:
                root = concept

    graph = get_cumfreq_branch(root, branch, ontology_graph_ct)
    best_branch = get_best_branch(root, graph)

    if len(best_branch) > 0:
        return best_branch, min(best_branch, key=lambda item: item[1])[0]

    return [], -1


def get_cumfreq_branch(curr, branch, ontology_graph_ct):
    graph = {
        curr: (branch[curr], [])
    }

    if curr not in ontology_graph_ct.keys():
        return graph

    for concept in branch.keys():
        if concept != curr and concept in ontology_graph_ct[curr]:
            graph[curr][1].append(concept)
            graph[concept] = (branch[concept], [])

    tmp = {}
    for subconcept in graph.keys():
        if subconcept != curr:
            sub_branch = get_cumfreq_branch(subconcept, branch, ontology_graph_ct)
            tmp[curr] = (graph[curr][0] + sub_branch[subconcept][0], graph[curr][1])
            tmp.update(sub_branch)

    graph.update(tmp)
    return graph


def get_best_branch(curr, graph):
    result = [(curr, graph[curr][0])]
    child = [
        (child, graph[child][0])
        for child in graph[curr][1]
    ]

    if len(child) > 0:
        best_child = get_best_branch(max(child, key=lambda item: item[1])[0], graph)
        result.extend(best_child)
        return result
    else:
        return result
"""
